import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  ssr: false,
  target: 'static',
  head: {
    titleTemplate: '%s - ncm',
    title: 'ncm',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Kanit&display=swap' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  env: {
    BASE_API: process.env.BASE_API.replace(/\/$/, "") || 'http://localhost:1337',
    TOKEN_API: process.env.TOKEN_API || ''
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '@assets/base.scss'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '@plugins/function',
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxt/components'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/bootstrap-vue',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: "AIzaSyCyHTFGRV8VpH9DzCCH_6EiVTkA7-ar544",
          authDomain: "ncmprojectweb.firebaseapp.com",
          databaseURL: "https://ncmprojectweb.firebaseio.com",
          projectId: "ncmprojectweb",
          storageBucket: "ncmprojectweb.appspot.com",
          messagingSenderId: "377685167215",
          appId: "1:377685167215:web:5f24c85d5f0b1b2439d19a",
          measurementId: "G-9BLVV64RKP"
        },
        services: {
          auth: {
            persistence: 'local',
            initialize: {
              onAuthStateChangedMutation: 'onAuthStateChangedAction',
              onAuthStateChangedAction: 'onAuthStateChangedAction'
            },
            ssr: false
          }
        }
      }
    ]
  ],
  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.sass'],
    treeShake: true,
    defaultAssets: {
      font: {
        family: 'Kanit'
      },
    },
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    extend(config) {
      config.resolve.alias['vue'] = 'vue/dist/vue.common'
    }
  }
}
