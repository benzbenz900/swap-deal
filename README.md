# ncm

```bash
ENV

BASE_API = http://localhost:1337
TOKEN_API = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmNmE0ODEzMzBlZmE3MDMwNDQ4NThhMyIsImlhdCI6MTYwMDg3NDk4MCwiZXhwIjoxNjAzNDY2OTgwfQ.ctnX3kzKlfQjverete-Y8wN-erGgL5otLuDdiFU9nPY

```

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
