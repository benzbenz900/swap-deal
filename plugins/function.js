const TOKEN_API = process.env.TOKEN_API
import Vue from 'vue'

const getImage = PATH => {
    return `${process.env.BASE_API}${PATH}`
}

Vue.filter('getImage', getImage)

export default (context, exports) => {
    exports('getApi', async (PATH) => {
        const data = await context.$axios({
            method: 'get',
            url: `${process.env.BASE_API}/${PATH}`,
            headers: {
                'Authorization': `Bearer ${TOKEN_API}`
            }
        })
        return data.data
    })

    exports('gql', async (query) => {
        const data = await context.$axios({
            method: 'post',
            url: `${process.env.BASE_API}/graphql`,
            headers: {
                'Authorization': `Bearer ${TOKEN_API}`,
                'Content-Type': 'application/json'
            },
            data: JSON.stringify({
                query: query,
                variables: {}
            })
        })
        return data.data
    })

    exports('getImage', getImage)
}