import Vuex from "vuex"

export default () => {
    return new Vuex.Store({
        strict: process.env.NODE_ENV !== 'production',
        state: {
            userdata: false,
            dialog_signin: false,
            category: null
        },
        mutations: {
            setdialog_signin(state, data) {
                state.dialog_signin = data
            },
            setUserData(state, data) {
                state.userdata = data
            },
            onAuthStateChangedAction(state, data) {
                state.userdata = (data.authUser != null) ? data.authUser.uid : false
            },
            setCategoryLoad(state, data) {
                state.category = data
            }
        },
        actions: {
            async nuxtServerInit(vuexContext) {

            },
            onAuthStateChangedAction(vuexContext, data) {
                vuexContext.commit("setUserData", (data.authUser != null) ? data.authUser.uid : false)
            },
        },
        getters: {
            isLogin(state) {
                return state.userdata ? true : false
            },
            allstate(state) {
                return state
            }
        }
    });
}